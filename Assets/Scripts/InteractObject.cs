﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractObject : MonoBehaviour
{
    private Camera cam;
    private GameObject objectHit;
    private HighlightObject hitMaterial;
    public bool clicked;
    public bool hovering;
    [Range(0, 20)]
    public float reach;

    void Start()
    {
        cam = Camera.main;
    }

    void FixedUpdate()
    {
        Interaction();
    }

    public void Interaction()
    {
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        RaycastHit hit;
        Debug.DrawRay(cam.transform.position, cam.transform.forward * reach, Color.blue);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.CompareTag("Interactable") && hit.distance < reach)
            {
                if (objectHit == null || objectHit.name != hit.transform.gameObject.name)
                {
                    hovering = true;
                    objectHit = hit.transform.gameObject;
                    hitMaterial = objectHit.GetComponent<HighlightObject>();

                    if (!clicked && (hitMaterial.currentMaterial != HighlightObject.Type.Highlight)) //if not highlight material not set
                    {
                        hitMaterial.setMaterial(HighlightObject.Type.Highlight);
                    }


                }
                if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.E))  //If not selected material not set
                {
                    hitMaterial.setMaterial(HighlightObject.Type.Selected);
                    FindObjectOfType<MainManager>().setSelectedObject(objectHit);
                    clicked = true;
                }
            }
            else
            {
                if (hovering && !clicked)
                {
                    objectHit.GetComponent<HighlightObject>().setMaterial(HighlightObject.Type.Normal);
                    objectHit = null;
                    hovering = false;
                }
                if (clicked && Input.GetMouseButtonDown(0))
                {
                    objectHit.GetComponent<HighlightObject>().setMaterial(HighlightObject.Type.Normal);
                    objectHit = null;
                    hovering = false;
                    clicked = false;
                }
            }
        }
    }
}

