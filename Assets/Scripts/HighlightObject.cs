﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class HighlightObject : MonoBehaviour
{
    public Targets.Target objectType;
    public Type currentMaterial;
    private Material selectedMat;
    private Material[] normalMaterials;
    public Material highlight;
    public Material selected;
    private Material normal;
    public enum Type
    {
        Normal,
        Selected,
        Highlight
    }
    private MeshRenderer rend;
    // Use this for initialization
    void Start()
    {
        rend = GetComponent<MeshRenderer>();
        normal = rend.material;
        normalMaterials = new Material[rend.materials.Length];
        Assert.IsNotNull(highlight, "Highlight not set on " + gameObject.name);
        Assert.IsNotNull(selected, "Selected not set on " + gameObject.name);

        for (int i = 0; i < rend.materials.Length; i++)
        {
            normalMaterials[i] = rend.materials[i];
        }
    }

    public void setMaterial(Type type)
    {
        currentMaterial = type;
        switch (type)
        {
            case Type.Highlight:
                selectedMat = highlight;
                break;
            case Type.Selected:
                selectedMat = selected;
                break;
            case Type.Normal:
                selectedMat = normal;
                break;
            default:
                break;
        }
        if (type != Type.Normal)
        {
            Material[] mats = rend.materials;
            for (int i = 0; i < rend.materials.Length; i++)
            {
                mats[i] = selectedMat;
            }
            rend.materials = mats;
        }
        else
        {
            rend.materials = normalMaterials;
        }
    }
}
