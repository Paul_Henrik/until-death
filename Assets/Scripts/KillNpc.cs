﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillNpc : MonoBehaviour {

void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("NPC"))
        {
            Debug.Log("Hit");
            other.gameObject.GetComponent<NPCmovement>().dead = true;
        }
    }
}
