﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCmovement : MonoBehaviour
{
    public GameObject[] destinations;
    public Transform currentDestination;
    public NavMeshAgent npcNavMesh;
    public bool dead;
    bool pause = false;
    
	void Start ()
    {
        npcNavMesh = GetComponent<NavMeshAgent>();
        destinations = GameObject.FindGameObjectsWithTag("Interactable");
        int temp = Random.Range(0, destinations.Length);
        currentDestination = destinations[temp].transform;
        npcNavMesh.SetDestination(currentDestination.position);
    }
	void Update()
    {
        die();
    }
	void FixedUpdate ()
    {
        Vector3 distance = currentDestination.position - transform.position;
        if (distance.magnitude < 3.5f && !pause)
        {
            pause = true;
            StartCoroutine(waitTime());
        }
	}

    IEnumerator waitTime()
    {
        yield return new WaitForSeconds(3);
        int temp = Random.Range(0, destinations.Length);
        currentDestination = destinations[temp].transform;
        npcNavMesh.SetDestination(currentDestination.position);
        pause = false;
    }

    public void die()
    {
        if(dead)
        {
            GameObject.Find("Manager").GetComponent<MainManager>().death = true;
        }
    }

}
