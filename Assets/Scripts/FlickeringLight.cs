﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeringLight : MonoBehaviour
{

    public float minFlickering;
    public float maxFlickering;
    public bool activateFlickering;
    private bool flick;
    private float lightIntensity;
    private Light light;
    // Update is called once per frame

    void Start()
    {
        flick = false;
        light = GetComponent<Light>();
        lightIntensity = light.intensity;
    }
    void Update()
    {
        if (activateFlickering)
        {
            if (!flick)
            {
                StartCoroutine("Flicker");
            }
        }
    }

    private IEnumerator Flicker()
    {
        flick = true;
        light.intensity = Random.Range(minFlickering, maxFlickering);
        yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
        light.intensity = lightIntensity;
        yield return new WaitForSeconds(Random.Range(0, 5));
        flick = false;
    }



}
