﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class MainManager : MonoBehaviour
{
    public GameObject selectedObject;
    public AudioSource pianoMusic;
    public AudioSource radioMusic;
    bool actionSet = false;
    Targets.Target target;
    public GameObject human;
    public bool death = false;
    public Image fadeToBlack;
    bool fadeIn = true;
    bool shelfFallen = false;
    


    void Start()
    {
        human = GameObject.Find("NPC_human");
        Display.displays[1].Activate();
    }

    void Update()
    {
        if (selectedObject != null && !actionSet)
        {
            switch (target)
            {
                case Targets.Target.Window:
                    WindowInteract();
                    break;
                case Targets.Target.Piano:
                    PianoInteract();
                    break;
                case Targets.Target.Radio:
                    RadioInteract();
                    break;
                case Targets.Target.Picture:
                    PictureInteract();
                    break;
                case Targets.Target.Knife:
                    break;
                case Targets.Target.BookCase:
                    BookshelfInteract();
                    break;
                case Targets.Target.Candles:
                    CandlesInteract();
                    break;
                case Targets.Target.Lamp:
                    break;
                default:
                    break;
            }
            actionSet = true;
        }
        
        if (death)
        {
            EndGame();
        }
    }

    public void setSelectedObject(GameObject sendIn)
    {
        selectedObject = sendIn;
        target = selectedObject.GetComponent<HighlightObject>().objectType;
        actionSet = false;
    }

    void PianoInteract()
    {
        if (!pianoMusic.isPlaying)
        {
            pianoMusic.Play();
        }
        else
            pianoMusic.Stop();
    }

    void RadioInteract()
    {
        if (!radioMusic.isPlaying)
        {
            radioMusic.Play();
        }
        else
            radioMusic.Stop();
    }

    void WindowInteract()
    {

    }

    void KnifeInteract()
    {

    }

    void FruitInteract()
    {

    }

    void LampInteract()
    {

    }

    void BookshelfInteract()
    {
        if (!shelfFallen)
        {
            shelfFallen = true;
            selectedObject.transform.parent.Rotate(0, 0, 90);
            selectedObject.GetComponent<BURNEVEYTHING>().STARTFIRE();
            selectedObject.GetComponent<NavMeshObstacle>().enabled = false;
        }
    }

    void CandlesInteract()
    {
        selectedObject.gameObject.GetComponent<BURNEVEYTHING>().STARTFIRE();
    }

    void PictureInteract()
    {

    }

    void HumanDie()
    {
        human.transform.Rotate(90, 0, 0);
        death = true;
    }

    void EndGame()
    {
        if (fadeToBlack.color.a < 1 && fadeIn)
        {
            Color temp = fadeToBlack.color;
            temp.a += 0.02f;
            fadeToBlack.color = temp;
        }
        else if (!fadeIn)
        {
            Color temp = fadeToBlack.color;
            temp.a -= 0.02f;
            fadeToBlack.color = temp;
        }

        if (fadeToBlack.color.a >= 1 && fadeIn)
        {
            human.transform.Rotate(-90, 0, 0);
            Color temp = Color.white;
            temp.a = 0.5f;
            human.GetComponent<Renderer>().material.color = temp;
            fadeIn = false;
        }


    }
}
