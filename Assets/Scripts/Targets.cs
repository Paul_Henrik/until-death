﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targets : MonoBehaviour {

    public Transform[] TargetObject;
    public int currentTarget;
    public enum Target
    {
        Window = 0,
        Piano= 1,
        Radio = 2,
        Picture = 3,
        Knife = 4,
        BookCase = 5,
        Candles = 6,
        Lamp = 7
    }

    public Transform GoToTarget(Target target)
    {
        currentTarget = (int)target;
        return TargetObject[currentTarget];
    }
}
