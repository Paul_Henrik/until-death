﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector3 moveDir;
    Vector3 moveAmount;
    Vector3 smoothVelocity;
    [Range(0, 20)]
    public float speed;
    public Camera mainCam;

    [Range(0, 20)]
    public float mouseSensetivityX;
    [Range(0, 20)]
    public float mouseSensetivityY;
    float verticalLookRotation;
    
	void Start ()
    {
        mainCam = Camera.main;
	}

    void Update()
    {
        moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        
        verticalLookRotation += Input.GetAxis("Mouse Y") * mouseSensetivityY;
        verticalLookRotation = Mathf.Clamp(verticalLookRotation, -25, 25);
        mainCam.transform.localEulerAngles = Vector3.left * verticalLookRotation;
    }

	void FixedUpdate()
    {
        transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * mouseSensetivityX);
        transform.Translate(moveDir * speed * Time.fixedDeltaTime);
    }
}
