﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Picture : MonoBehaviour {

    public Vector3 spawnPos;
    public GameObject prefabObject;
    private GameObject spawnObject;
    public float speed;
    [Range(0,2)]
    public float pivotOffset;
    private bool moving;
    private Vector3 target;
    private bool objectSpawned;
    public bool spawnObjectBool;
    private float step;
    public bool ToDaFace;

    void Start()
    {
        spawnObjectBool = true;
        ToDaFace = true;
    }

    void Update()
    {
        if(objectSpawned)
        {
            movingObject();
        }
        if(spawnObjectBool)
        {
            spawnObjectBool = false;
            InstantiateObject();
        }
    }

    public void InstantiateObject()
    {
        spawnPos = Camera.main.transform.position + (Camera.main.transform.forward * 5) ;
        spawnObject = Instantiate(prefabObject, spawnPos, Quaternion.identity, gameObject.transform);
        spawnObject.transform.localScale -= new Vector3(0.99f, 0.99f, 0.99f); 
        //spawnObject.transform.rotation = spawnObject.transform.parent.rotation; // you just set rotation to identity tho
        objectSpawned = true; // hilsn Steinar <3
    }

    public void movingObject()
    {
        if(ToDaFace)
        {
        Debug.Log(Camera.main.transform.position);
        target = (Camera.main.transform.position - new Vector3(0, pivotOffset,0)) + (Camera.main.transform.forward * 1);
        step = speed * Time.deltaTime;
        spawnObject.transform.position = Vector3.MoveTowards(spawnObject.transform.position, target, step);
        }
        else
        {
            step = speed * Time.deltaTime;
            target = FindObjectOfType<MainManager>().GetComponent<Targets>().GoToTarget(Targets.Target.Window).position;
            spawnObject.transform.position = Vector3.MoveTowards(spawnObject.transform.position, target, step);
            spawnObject.transform.parent = null;
        }
    }
}
